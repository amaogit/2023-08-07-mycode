Role Name: poweredge-system-inventory
=========

A brief description of the role goes here.

Requirements
------------

- Need omsdk for collection named dellemc.openmanage by issue command: python3 -m pip install omsdk
- You need pass idrac_user for iDRAC login name and idrac_password for iDRAC user password as EXTRA VARS

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: all
      roles:
         - { role: poweredge-system-inventory, become: false }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
